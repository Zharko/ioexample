#include <iostream>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>

namespace nspace
{
  // { "key1": 1.0d, "key2": "Destiny is all." }
  struct Data
  {
    double key1;
    std::string key2;
  };

  struct DelimeterIO {
    char exp;
  };
  struct DoubleIO {
    double & ref;
  };
  struct StringIO {
    std::string & ref;
  };
  struct LabelIO {
    std::string exp;
  };
  class iofmtguard {
   public:
    iofmtguard(std::basic_ios< char > & s);
    ~iofmtguard();
   private:
    std::basic_ios< char > & s_;
    char fill_;
    std::basic_ios< char >::fmtflags fmt_;
  };
  std::istream & operator>>(std::istream & in, DelimeterIO && dest);
  std::istream & operator>>(std::istream & in, DoubleIO && dest);
  std::istream & operator>>(std::istream & in, StringIO && dest);
  std::istream & operator>>(std::istream & in, LabelIO && dest);
  std::istream & operator>>(std::istream & in, Data & dest);
  std::ostream & operator<<(std::ostream & out, const Data & dest);
}

int main()
{
  using nspace::Data;
  std::vector< Data > data;
  std::copy(
   std::istream_iterator< Data >(std::cin),
   std::istream_iterator< Data >(),
   std::back_inserter(data)
  );
  std::cout << "Data:\n";
  std::copy(
   std::begin(data),
   std::end(data),
   std::ostream_iterator< Data >(std::cout, "\n")
  );
}

namespace nspace
{
  std::istream & operator>>(std::istream & in, DelimeterIO && dest)
  {
    std::istream::sentry sentry(in);
    if (!sentry)
    {
      return in;
    }
    char c = '0';
    in >> c;
    if (in && (c != dest.exp))
    {
      in.setstate(std::ios::failbit);
    }
    return in;
  }

  std::istream & operator>>(std::istream & in, DoubleIO && dest)
  {
    std::istream::sentry sentry(in);
    if (!sentry)
    {
      return in;
    }
    return in >> dest.ref >> DelimeterIO{'d'};
  }

  std::istream & operator>>(std::istream & in, StringIO && dest)
  {
    std::istream::sentry sentry(in);
    if (!sentry)
    {
      return in;
    }
    return std::getline(in >> DelimeterIO{'"'}, dest.ref, '"');
  }

  std::istream & operator>>(std::istream & in, LabelIO && dest)
  {
    std::istream::sentry sentry(in);
    if (!sentry)
    {
      return in;
    }
    std::string data = "";
    if ((in >> StringIO{data}) && (data != dest.exp))
    {
      in.setstate(std::ios::failbit);
    }
    return in;
  }

  std::istream & operator>>(std::istream & in, Data & dest)
  {
    std::istream::sentry sentry(in);
    if (!sentry)
    {
      return in;
    }
    Data input;
    {
      using sep = DelimeterIO;
      using label = LabelIO;
      using dbl = DoubleIO;
      using str = StringIO;
      in >> sep{'{'};
      in >> label{"key1"} >> sep{':'} >> dbl{input.key1};
      in >> sep{','};
      in >> label{"key2"} >> sep{':'} >> str{input.key2};
      in >> sep{'}'};
    }
    if (in)
    {
      dest = input;
    }
    return in;
  }

  std::ostream & operator<<(std::ostream & out, const Data & src)
  {
    std::ostream::sentry sentry(out);
    if (!sentry)
    {
      return out;
    }
    iofmtguard fmtguard(out);
    out << "{ ";
    out << "\"key1\": " << std::fixed << std::setprecision(1) << src.key1 << "d, ";
    out << "\"key2\": " << src.key2;
    out << " }";
    return out;
  }

  iofmtguard::iofmtguard(std::basic_ios< char > & s):
    s_(s),
    fill_(s.fill()),
    fmt_(s.flags())
  {}

  iofmtguard::~iofmtguard()
  {
    s_.fill(fill_);
    s_.flags(fmt_);
  }
}